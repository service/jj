# TIP

一个把便捷编码作为目标的 ORM 框架

#### 生成模型

下载(编译)和安装 jj 命令工具到系统 path 下, 执行命令查看帮助 jj gen:entity -h

https://gitcode.com/service/jj/overview

## 示例

下载(编译)和安装 jj 命令工具到系统 path 下, 执行命令查看帮助 jj gen:entity -h

https://gitcode.com/service/jj/overview

已 Mac 系统作为例子, 执行如下命令安装 jj 到系统任意 path 下

```bash
cjpm build -o jj && cp ./target/release/bin/jj /Applications/cangjie/bin/jj
```

```bash
 ~/Desktop/gitcode.com/service/jj $ jj
0

     ██╗     ██╗
     ██║     ██║
     ██║     ██║
██   ██║██   ██║
╚█████╔╝╚█████╔╝
 ╚════╝  ╚════╝  v0.0.1

Usage:
  command [options] [arguments] [has]
Base Options:
  -h                全局选项, 显示帮助信息, 缩写模式
  -help             全局选项, 显示帮助信息
Available commands:
  gen:entity        生成JORM框架的辅助代码, 实体类和模型类
  help              默认的帮助命令
 ~/Desktop/gitcode.com/service/jj $ jj gen:entity -h
0
Usage:
  command [options] [arguments] [has]
Description:
  生成JORM框架的辅助代码, 实体类和模型类
Options:
  -host             default[127.0.0.1]            数据库IP地址
  -port             default[3306]                 数据库端口
  -user             default[root]                 数据库用户名
  -password         default[root]                 数据库密码
  -database         default[temp]                 数据库名称
  -out              default[./entity/src]         模型输出目录
  -packageName      default[entity]               模型的包名
```

## TODO

- 多种数据库驱动支持
- pgsql 实体支持
